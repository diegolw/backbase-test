# `BackBase` - Frontend Developer Exercise 2.5

Thank you for this opportunity to show my work. I really appreciate it.
I hope you enjoy it, I did it with passion and care.


## Getting Started

All the dependencies are referenced to CDNs, to avoid load extra files into this repo.
To get you started you can simply copy the application code into a web server folder,
or in case of using http-server, just run it.

### Installing http-server (just in case you don't have any web server)

```
npm install http-server -g
```

### Run

In the /app folder:

```
http-server
```


## Directory Layout

```
app/                  --> all of the source files for the application
    list/                 --> the list view template and logic
        list.html             --> the partial template
        list.js               --> the controller logic
    map/                  --> the map view template and logic
        map.html              --> the partial template
        map.js                --> the controller logic
    service/              --> the parking service
        parkings.js           --> consumes the API and normalizes the data
    app.css               --> app stylesheet
    app.js                --> main application module
    index.html            --> app layout file (the main html template file of the app)
```


## Implementation

Separating the concerns of parkings list and parking map, I've divided them into this two modules (	'BB-Parking.list', 'BB-Parking.map'), using 'ngRoute' to switch between them. Each module is responsible for your own routes, controller and view.
The only project dependency is the lib 'uiGmapgoogle-maps' and their dependencies.

### Module `list`

**Filter type short and long**
As we have the properties FreeSpaceShort and FreeSpaceLong, I've decided to create a filter where you can toggle between both and instantly shows you their free space availability.

**Color table**
Studing the API, I came across with "State" property. I was not sure what it meant (its value could be "ok", "warning" and "error"). As I needed to use "red" and "green" on the table, I used "red" to represent "no available spots" and "green" to represent the contrary.

**Pagination**
I've decided to implement the pagination with **<<** and **>>** characters because I
was unable to know how much pages would be available dynamically. I mean, API **?count** param would determine whether the API should count the total amount or available results, the resulting number will be available in the X-Result-Count HTTP header. However, the custom headers will be visible in same domain. For the crossdomain situation, the server has to send Access-Control-Expose-Headers: X-Result-Count header to make the custom headers visible. And it is not happening.
I could work around that using JSONP, but I thought I wouldn't be an elegant solution.

### Module `map`

Shows the available spots both short and long.
Places the google marker in correct place.

### Factory `parkings`

Service that performs load and synchronization of the parking spots, using CitySDK's API.
Working with our parking model I've decided to use $http to load data from the API (this is built into Angular, so there's no need for the extra overhead of loading in an external dependency like $resouce, that is good for situations that are slightly more complex than $http. It's good when you have pretty structured data).
I have also used **polling** technique to get data updated every 5 seconds. I did it thinking that someone can let the browser opened and get data updated without the necessity to reload the page every time he needs. I've set 5 seconds because I'm not sure how frequency the API is updated. =)
It has two methods: **get** (it used to get an specific spot, passing its ID as param) and **page** (returns 10 items from the API).

### Routes

I'm routing this application throw the two views "list" and "map".
List defines a route **/list/:page** and map defines **/spot/:id**. This way we can access the application from whatever any existing page and spot id. It also turns all the application URLs sharable.