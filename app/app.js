'use strict';

// Load BB-Parking's dependencies
angular.module('BB-Parking', [
	'ngRoute',
	'BB-Parking.list',
	'BB-Parking.map',
	'uiGmapgoogle-maps'
])

// Route for the BB-Parking app, main location
.config(['$locationProvider', '$routeProvider',
function ($locationProvider, $routeProvider) {
	$locationProvider.hashPrefix('!');
	// Dispatchs to the list module
	$routeProvider.otherwise({redirectTo: '/list/1'});
}]);