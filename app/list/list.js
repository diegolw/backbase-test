'use strict';


// Controller for the parking list
var ListCtrl = function ($routeParams, Parkings) {

	var ERROR_MSG = 'Unable to load parkings data page from ListCtrl.';

	// Page param
	var page = Number($routeParams.page);
	this.page = page;

	// Initialize type variable
	this.spotType = 'short';
	// Initialize next page
	this.nextPage = page;
	// Initialize previous page
	this.previousPage = page > 1 ?  page - 1 : page;

	// Consumes from service 'Parkings' a page
	Parkings.page(page, function success(parkings) {
		this.parkings = parkings;
		// Update next page in case exist more parkings
		this.nextPage = parkings.length === 10 ? page + 1 : page;
	}.bind(this), function error(response) {
		console.log(ERROR_MSG);
	});

	this.parkingClass = function (parking, spotType) {
		if (parking.FreeSpaceShort == 0 && spotType == 'short' ||
			parking.FreeSpaceLong == 0 && spotType == 'long') {
			return 'list-group-item-danger';
		}
	}
};


// List module
angular.module('BB-Parking.list', ['ngRoute'])

// Route for the parking list
.config(['$routeProvider', function ($routeProvider) {
	$routeProvider.when('/list/:page', {
		templateUrl: 'list/list.html',
		controller: 'ListCtrl as ctrl'
	});
}])
.controller('ListCtrl', ListCtrl);