'use strict';


// Controller for the parking list
var MapCtrl = function ($routeParams, Parkings, $window) {

    var ERROR_MSG = 'Unable to load parkings data from MapCtrl.';

    var id = $routeParams.id;

    // This can be asynchronous because you can access directly
    // through the URL. Eg: /spot/2
    Parkings.get(id, function success(parking) {
        this.parking = parking;
        var latitude = parking.latitude;
        var longitude = parking.longitude;

        this.map = {
            center: {
                latitude: latitude,
                longitude: longitude
            },
            markers: [{
                title: parking.Name,
                id: parking.id,
                latitude: latitude,
                longitude: longitude
            }],
            zoom: 15
        };
    }.bind(this), function error(response) {
        console.log(ERROR_MSG);
    });

    // Return button
    this.back = function () {
        $window.history.back();
    };

};


// Map module
var app = angular.module('BB-Parking.map', ['ngRoute'])

// Route for the parking map
.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/spot/:id', {
        templateUrl: 'map/map.html',
        controller: 'MapCtrl as ctrl'
    });
}])
.controller('MapCtrl', MapCtrl);