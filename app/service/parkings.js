'use strict';


// Service that performs load and synchronization of the parking spots,
// using CitySDK's API
angular.module('BB-Parking').factory('Parkings', function ($http, $timeout) {

	var URL = '//api.citysdk.waag.org/layers/parking.garage/objects?';
	var POLLING_TIME = 15e3; // 15 seconds

	// Updates only the information that is shown
	var polling = null;

	// Handles the API feature object
	function dataHandler(feature) {
		var properties = feature.properties;
		var garage = properties.layers['parking.garage'].data;
		var coordinates = feature.geometry.coordinates;
		var parking = garage;
		parking.id = properties.cdk_id;
		parking.latitude = coordinates[1];
		parking.longitude = coordinates[0];
		return parking;
	}

	// Handles the API response, normalizing the view's desired data
	function responseHandler(response) {
		var page = [];
		var features = response.data.features;
		for (var i = 0, max = features.length; i < max; i++) {
			var feature = features[i];
			var parking = dataHandler(feature);
			page.push(parking);
		}
		return page;
	}

	/** Returns a specific parking object asynchronously */
	function get(id, callbackSuccess, callbackError) {
		var url = URL + '&cdk_id=' + id;
		$http.get(url).then(function getSuccess(response) {
			var features = response.data.features;
			var feature = features[0];
			var parking = dataHandler(feature);
			callbackSuccess(parking);
			console.log('Parking data update at ' + new Date().toString());
			$timeout.cancel(polling);
			polling = $timeout(get.bind(this, id, callbackSuccess), POLLING_TIME);
		}, function getError(response) {
			console.error('Unable to load parking data.');
			callbackError(response);
		});
	}

	/** Returns a page of parking objects asynchronously */
	function page(number, callbackSuccess, callbackError) {
		var url = URL + '&page=' + number;
		$http.get(url).then(function pageSuccess(response) {
			var parkings = responseHandler(response);
			callbackSuccess(parkings);
			console.log('Parkings data update at ' + new Date().toString());
			$timeout.cancel(polling);
			polling = $timeout(page.bind(this, number, callbackSuccess), POLLING_TIME);
		}, function getError(response) {
			console.error('Unable to load parkings data page.');
			callbackError(response);
		});
	}

	var parkings = {
		get: get,
		page: page
	};

	return parkings;
});